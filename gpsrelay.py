import paho.mqtt.client as mqtt
import requests, time, json

mqttClient = mqtt.Client('GPS_RELAY_CLIENT_1')
broker = 'broker.emqx.io'

def read_directions(fileName):
    with open(fileName) as file:
        result = json.load(file)
        coords = result['features'][0]['geometry']['coordinates']
        return coords

def reconnect_mqtt():
    print("MQTT Client Disconnected")
    connect_mqtt()

def connect_mqtt():
    mqttClient.connect(broker)
    mqttClient.on_disconnect = reconnect_mqtt

def get_location():
    res = requests.get('https://wanderdrone.appspot.com')
    return res.json()['geometry']['coordinates']

def publish_location(topic, coordinates):
    mqttClient.publish(topic, f"{coordinates}")
    print(f"Sent {coordinates} to {topic}")

#print(get_location())
connect_mqtt()

coords = read_directions('results.json')
while 1:
    for coord in coords:
        publish_location('sawaTV/real', coord)
        time.sleep(2.5)