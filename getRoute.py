import json
from mapbox import Directions

def get_directions():
    waypoints = []
    with open('JujatoCBD.json') as file:
        coords = json.load(file)
        for i in coords:
            feature = dict()
            name = i['name']
            lng = i['center'][0]
            lat = i['center'][1]
            feature['type'] = 'Feature'
            feature['geometry'] = dict()
            feature['geometry']['type'] = 'Point'
            coord = [lng, lat]
            feature['geometry']['coordinates'] = coord
            waypoints.append(feature)
            #print(f"{name} - \t\t{lng}, \t\t{lat}")

    service = Directions(access_token='pk.eyJ1Ijoia2FyYW5qYW11dGFoaSIsImEiOiJja2c0dXFtMTkwb2J1MzNwNHdpbWk2dW5pIn0.UtXqlLEtuv87e_cxFaZepQ')
    response = service.directions(waypoints, steps=True, geometries='geojson', overview='full')

    return response.geojson()

def read_directions():
    with open('results.json') as file:
        result = json.load(file)
        coords = result['features'][0]['geometry']['coordinates']
        return coords

coords = read_directions()
for index, coord in enumerate(coords):
    print(f"{index+1}. {coord}")